#!/bin/bash

set +eo pipefail

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "$HERE/ci-libvirt-lib.sh"

virsh destroy "$CI_IMAGE_NAME"

# TODO: only remove all writable file disk storage instead
#       of assuming storage is on vda.
virsh undefine "$CI_IMAGE_NAME" --storage vda --snapshots-metadata

sync
