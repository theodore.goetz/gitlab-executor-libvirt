shopt -s expand_aliases

if [ -f ~/.bashrc ]; then
    . ~/.bashrc
fi

export SSH_OPTS=(
    -o LogLevel=ERROR
    -o StrictHostKeyChecking=no
    -o UserKnownHostsFile=/dev/null
)

export SSH_KEYFILE="$HOME/.ssh/id_rsa"

export SSH_PRIVATE_KEY_OPTS=(
    -i "$SSH_KEYFILE"
    -o PasswordAuthentication=no
    "${SSH_OPTS[@]}"
)

# Ensure command is bash array
eval CI_JOB_CMD=\("$CI_JOB_CMD"\)

# Connect to QEMU user-mode
export LIBVIRT_DEFAULT_URI='qemu:///session'

# Get IP address of a running VM using libvirt and arp.
function vm-ip-addr {
    NET_DEVICE="$1"
    DOMAIN="$2"

    MACADDR=$(virsh domiflist "$DOMAIN" | grep "$NET_DEVICE" | awk '{print $NF}')
    IPADDR=$(arp -ean | grep "$NET_DEVICE" | grep "$MACADDR" | awk '{print $1}')

    if [[ $IPADDR == "" ]]; then
        return 1
    fi
    echo "$IPADDR"
}

function log-level {
    case "$1" in
        critical) echo 50;;
        error) echo 40;;
        warning) echo 30;;
        info) echo 20;;
        debug) echo 10;;
        *) echo "$1";;
    esac
}

function log {
    LEVEL="$1"
    shift
    if [[ $(log-level "$LEVEL") -ge $(log-level "$CI_LOG_LEVEL") ]]; then
        echo "[$LEVEL]" "${@}" 1>&2
    fi
}
