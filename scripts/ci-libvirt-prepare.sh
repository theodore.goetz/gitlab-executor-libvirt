#!/bin/bash

set -eo pipefail
trap 'exit $SYSTEM_FAILURE_EXIT_CODE' ERR

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "$HERE/ci-libvirt-lib.sh"

log info "CI_JOB_IMAGE=$CI_JOB_IMAGE"
log debug "CI_JOB_CMD=$CI_JOB_CMD"
log debug "CI_IMAGE_OS=$CI_IMAGE_OS"
log debug "CI_IMAGE_NAME=$CI_IMAGE_NAME"
log debug "CI_SSH_USER=$CI_SSH_USER"
log debug "CI_NET_DEVICE=$CI_NET_DEVICE"

log debug "CI_BUILDS_DIR=$CI_BUILDS_DIR"
log debug "CI_CACHE_DIR=$CI_CACHE_DIR"

log debug "CI_BUILD_FAILURE_EXIT_CODES=$CI_BUILD_FAILURE_EXIT_CODES"
log debug "CI_SYSTEM_FAILURE_EXIT_CODES=$CI_SYSTEM_FAILURE_EXIT_CODES"
log debug "CI_DEFAULT_FAILURE_EXIT_CODE=$CI_DEFAULT_FAILURE_EXIT_CODE"

log debug "CI_TIMEOUT=$CI_TIMEOUT"
log debug "CI_LOG_LEVEL=$CI_LOG_LEVEL"

log debug "LIBVIRT_DEFAULT_URI=$LIBVIRT_DEFAULT_URI"

log debug "SSH_OPTS=$SSH_OPTS"
log debug "SSH_KEYFILE=$SSH_KEYFILE"
log debug "SSH_PRIVATE_KEY_OPTS=$SSH_PRIVATE_KEY_OPTS"

log debug "CI_JOB_CMD=${CI_JOB_CMD[@]}"

if [[ ! -f ~/.ssh/id_rsa ]]; then
    ssh-keygen -q -t rsa -N '' -f "$SSH_KEYFILE" <<<y
fi

if virsh dominfo "$CI_IMAGE_NAME" &>/dev/null; then
    log warning "Forcefully stopping and removing $CI_IMAGE_NAME image"
    virsh destroy "$CI_IMAGE_NAME" || true
    # TODO: remove all writable file disk storage
    virsh undefine "$CI_IMAGE_NAME" --storage vda --snapshots-metadata
fi

if ! virsh dominfo "$CI_JOB_IMAGE" &>/dev/null; then
    log error "The image $CI_JOB_IMAGE was not found in $QEMU_URI"
    exit $SYSTEM_FAILURE_EXIT_CODE
fi

log info "cloning $CI_JOB_IMAGE to $CI_IMAGE_NAME"
virt-clone --preserve-data --auto-clone --skip-copy vda \
    --original "$CI_JOB_IMAGE" --name "$CI_IMAGE_NAME"

# Make linked copies of all disk storage in original VM
while read -r DISK; do
    eval DISK=\("${DISK[*]}"\)
    if [[ "${DISK[0]}" == "file" && "${DISK[1]}" == "disk" ]]; then
        DEVICE="${DISK[2]}"
        BACKING_FILE="${DISK[3]}"
        NEW_FILE="$(dirname "$BACKING_FILE")/${CI_IMAGE_NAME}.qcow2"

        qemu-img create -q -f qcow2 -o "backing_file=$BACKING_FILE" "$NEW_FILE"

        virt-xml "$CI_IMAGE_NAME" --edit "target=$DEVICE" \
            --disk "readonly=,path=$NEW_FILE"
    fi
done < <(virsh --quiet domblklist "$CI_IMAGE_NAME" --details)

if [[ "$CI_IMAGE_OS" == "linux" ]]; then
    log debug 'Provisioning the VM for CI job.'

    # ensure the VM gets a new IP address by clearing out all network related settings
    virt-sysprep -d "$CI_IMAGE_NAME" --hostname "$CI_IMAGE_NAME" \
        --operations dhcp-client-state,dhcp-server-state,net-hostname,net-hwaddr,udev-persistent-net,machine-id

    eval CI_JOB_CMD=\("${CI_JOB_CMD[*]}"\)
    CMD=$(basename "${CI_JOB_CMD[0]}")
    virt-customize -d "$CI_IMAGE_NAME" \
        --install "${CMD},git,git-lfs,gitlab-runner" \
        --write "/etc/hostname:$CI_IMAGE_NAME" \
        --ssh-inject "$CI_SSH_USER" \
        --mkdir "$CI_BUILDS_DIR" --chmod 0777:"$CI_BUILDS_DIR" \
        --mkdir "$CI_CACHE_DIR" --chmod 0777:"$CI_CACHE_DIR"
fi

virsh start "$CI_IMAGE_NAME"

log debug "Waiting for VM to get an IP address."
COUNT=0
MAXCOUNT=120
until IPADDR=$(vm-ip-addr "$CI_NET_DEVICE" "$CI_IMAGE_NAME"); do
    if [[ $((COUNT++)) -gt $MAXCOUNT ]]; then
        log error "VM could not obtain an IP address."
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi
    sleep 1
done
log debug "IP address: $IPADDR"

alias sshpass="sshpass -p '$CI_SSH_PASSWORD'"
alias sshpass-exec="sshpass ssh ${SSH_OPTS[*]} $CI_SSH_USER@$IPADDR"

log debug "Waiting for VM to be listening for SSH connections."
COUNT=0
MAXCOUNT=60
until sshpass-exec true &>/dev/null; do
    if [[ $((COUNT++)) -gt $MAXCOUNT ]]; then
        log error "Could not ssh into VM."
        sshpass-exec -vv "$CI_SSH_USER@$IPADDR" true
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi
    sleep 1
done
log debug "SSH connection to $CI_IMAGE_NAME succeeded on $CI_SSH_USER@$IPADDR"

alias ssh-exec="ssh ${SSH_PRIVATE_KEY_OPTS[*]} $CI_SSH_USER@$IPADDR"

# Setup ssh key authentication for windows
if [[ "$CI_IMAGE_OS" == "windows" ]]; then
    if ! ssh-exec true &>/dev/null; then
        sshpass ssh-copy-id -i "$SSH_KEYFILE" "${SSH_OPTS[@]}" "$CI_SSH_USER@$IPADDR"
        sshpass-exec 'cp /root/.ssh/authorized_keys /mnt/c/ProgramData/ssh/administrators_authorized_keys'
        sshpass-exec 'icacls.exe "C:\ProgramData\ssh\administrators_authorized_keys" /remove "NT AUTHORITY\Authenticated Users"'
        sshpass-exec 'icacls.exe "C:\ProgramData\ssh\administrators_authorized_keys" /inheritance:r'
        sshpass-exec 'powershell.exe -Command "Get-Acl \"C:\ProgramData\ssh\ssh_host_dsa_key\" | Set-Acl \"C:\ProgramData\ssh\administrators_authorized_keys\""'
    fi
fi

unalias sshpass-exec
unalias sshpass

log debug "Attempting to ssh into VM using keyfile."
until ssh-exec true &>/dev/null; do
    if [[ $((COUNT++)) -gt $MAXCOUNT ]]; then
        log error "Could not ssh into VM using keyfile."
        ssh-exec -vv "$CI_SSH_USER@$IPADDR" true
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi
    sleep 1
done
log info "SSH connection to $CI_IMAGE_NAME succeeded on $CI_SSH_USER@$IPADDR using ssh keyfile"

if [[ "$CI_IMAGE_OS" == "windows" ]]; then
    # TODO: ensure that git, git-lfs, gitlab-runner and $(basename "${CI_JOB_CMD[0]}") are installed

    ssh-exec mkdir -p "$CI_BUILDS_DIR" "$CI_CACHE_DIR"
fi
